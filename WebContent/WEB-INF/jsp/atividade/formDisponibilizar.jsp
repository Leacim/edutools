<%-- 
    Document   : formDisponibilizar
    Created on : 12/07/2014, 16:45:37
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <script>
            function disponibilizarAtividade() {
                if($("#formCadastroAtividade").validate().form() === true){
                $.ajax({type: 'POST', url: '<c:url value="/atividade/disponibilizar"/>', data: $('#formCadastroAtividade').serialize(), success: function(response) {
                       $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    document.getElementById("formCadastroAluno").reset();
                                }
                            }

                        });
                        document.getElementById("formCadastroAluno").reset();
                    }});
            }
                return false;
            }
        </script>
    </head>
    <body>
                <div style="width: 350px;">
                    <form  id="formCadastroAtividade" onsubmit="return disponibilizarAtividade();">
                    <p style="width: 400px;" class="campo"><label class="lab" style="padding-right: 40px;">Turma:</label>
                       <select  class="input" style="width: 228px; height:35px " name="turma.idTurma">
                                <option value="">Nenhuma</option>
                                <c:forEach var="turma" items="${turmas}">
                                    <option value="${turma.idTurma}">${turma.nome}</option>                    
                                </c:forEach>
                            </select>
                    <p style="width: 400px;" class="campo"><label class="lab" style="padding-right: 40px;">Atividade:</label>
                        <select  class="input" style="width: 228px; height:35px " name="atividade.idAtividade">
                                <option value="">Nenhuma</option>
                                <c:forEach var="atividade" items="${atividades}">
                                    <option value="${atividade.idAtividade}">${atividade.nome}</option>
                                </c:forEach>
                            </select>
                    <p></p>
                    
                    <button class="botao" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000">Cancelar</button>
                    <button class="botao" style="float:right; width: 120px;" onclick="$('#formCadastroAtividade').submit();">Cadastrar</button>
                    </form>
                </div>
                    <div id="dialog" title="Sucesso!" style="display: none">
            <p>Atividade disponibilizada!</p>
        </div>
    </body>
    <script>
        $("#formCadastroAtividade").validate({
            rules: {
                "atividade.idAtividade": {
                    required: true
                },
                "turma.idTurma": {
                    required: true
                }
            }
        });
    </script>
</html>
