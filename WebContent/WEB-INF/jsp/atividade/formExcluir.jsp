<%-- 
    Document   : formExcluir
    Created on : 12/07/2014, 16:36:11
    Author     : Samuel Romeiro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script>
            function cadastroAluno() {
                if($("#formCadastroAluno").validate().form() === true){
                $.ajax({type: 'POST', url: '<c:url value="/usuario/criar"/>', data: $('#formCadastroAluno').serialize(), success: function(response) {
                       $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    document.getElementById("formCadastroAluno").reset();
                                }
                            }

                        });
                        document.getElementById("formCadastroAluno").reset();
                    }});
            }
                return false;
            }
        </script>
    </head>
    <body>
        <div style="width: 350px;">
            <div style="width: 350px;">
                <p style="width: 400px;" class="campo"><label class="lab" style="padding-right: 40px;">Atividade:</label>
                    <select  class="input" style="width: 228px; height:35px " name="atividade.idAtividade">
                                <option value="">Nenhuma</option>
                                <c:forEach var="atividade" items="${atividades}">
                                    <option value="${atividade.idAtividade}">${atividade.nome}</option>                    
                                </c:forEach>
                            </select>
                <p></p>
                <button class="botao" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000">Cancelar</button>
                <button class="botao" style="float:right; width: 120px;">Excluir</button>
            </div>
        </div>
    </body>
</html>
