<%-- 
    Document   : atividades
    Created on : 06/07/2014, 16:43:26
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edutools</title>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
    </head>
    <body style="background-color: #cccccc; margin: 0px;">
        <div id="conteudo" style="min-height: 630px;">
            <div id="bannerLogado">
                <img style="cursor: pointer; padding-left: 20px;" width="420" height="110" src="<c:url value="/imagens/EduToolsAtividades.png" />" onclick="javascript:location = '<c:url value="/"/>';"/>
            </div>
            <div id="avatar">
                <label style="cursor: pointer;" onclick="javascript:location = '<c:url value="/usuario/logout"/>';">
                <img width="30" src="<c:url value="/imagens/Sair.png" />"/> Sair </label>
            </div>

            <div id="atividades" style="width: 1215px;"><%--<%@include file="../index/abas.jsp" %>--%>
                
                <div class="formatacaoAtividades">
                    <div class="escolhaAtividades" onclick="javascript:location = '<c:url value="/atividade/historia"/>';">
                        <img width="180" height="180" src="<c:url value="/imagens/atividades/icone_historia.jpg" />"/>
                    <label align="center" style="padding-left: 50px;" ><b>Histórias</b></label>
                    </div>
                    
                    
                </div>
                <div class="formatacaoAtividades">                    
                    <div class="escolhaAtividades">
                         <img width="180" height="180" src="<c:url value="/imagens/atividades/indisponivel.jpg" />"/>
                    <label align="center" style="padding-left: 50px;" ><b>Indisponível</b></label>
                    </div>

                </div>
                <div class="formatacaoAtividades">
                    <div class="escolhaAtividades">
                          <img width="180" height="180" src="<c:url value="/imagens/atividades/indisponivel.jpg" />"/>
                    <label align="center" style="padding-left: 50px;" ><b>Indisponível</b></label>
                    </div>
                       
                </div>
                    
                <div class="formatacaoAtividades">                    
                    <div class="escolhaAtividades">
                         <img width="180" height="180" src="<c:url value="/imagens/atividades/indisponivel.jpg" />"/>
                    <label align="center" style="padding-left: 50px;" ><b>Indisponível</b></label>
                    </div>
                </div>
            </div>

        </div>  
        <div id="rodape"><%@include file="../index/rodape.jsp" %></div>
    </body>
