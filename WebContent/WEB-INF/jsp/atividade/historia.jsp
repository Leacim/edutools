<%-- 
    Document   : atividades
    Created on : 06/07/2014, 16:43:26
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edutools</title>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/atividades.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.theme.css" />" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
        <script src="<c:url value="/js/jquery-ui.js" />" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function() {

                $("#quadro-2").hide();
                $("#quadro-3").hide();
                $("#quadro-4").hide();
                $("#quadro-final").hide();
            });
            function enviarResposta() {
                $.ajax({type: 'POST', url: '<c:url value="/atividade/validarHistoria"/>', data: $('#formValidarAtividade').serialize(),
                    success: function(response) {
                        /*  alert("Parabéns, Atividade Concluida!");*/
                         $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                   /* javascript:location = '<c:url value="/"/>';*/
                                    javascript:location = '<c:url value="/atividades"/>';
                                }
                            }

                        });       
                    },
                    error: function(response) {
                    $("#dialog2").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                }
                            }

                        });  
                        /* alert("Resposta Incorreta, tente mais uma vez.");*/
                    }
                });
                return false;
            }
        </script>
    </head>
    <body style="background-color: #cccccc; margin: 0px;">
        <div id="conteudo" style="min-height: 630px;">
            <div id="bannerLogado">
                <img style="cursor: pointer; padding-left: 20px;" width="420" height="110" src="<c:url value="/imagens/EduToolsAtividades.png" />" onclick="javascript:location = '<c:url value="/"/>';"/>
            </div>
            <div id="avatar">
                <label style="cursor: pointer;" onclick="javascript:location = '<c:url value="/usuario/logout"/>';">
                    <img width="30" src="<c:url value="/imagens/Sair.png" />"/> Sair </label>
            </div>

            <div id="atividades" style="width: 1215px;"><%--<%@include file="../index/abas.jsp" %>--%>

                <div id="quadro-1" style="padding-top: 200px;">
                    <div class="quadros_atividades" >
                        <img width="350" height="350" src="<c:url value="/imagens/atividades/q1.jpg" />"/>  
                    </div>
                    <p align="center">
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-1').hide();
                                $('#quadro-2').show();">Próximo</button>                
                    </p>
                </div>

                <div id="quadro-2" style="padding-top: 200px;">
                    <div class="quadros_atividades" >
                        <img width="350" height="350" src="<c:url value="/imagens/atividades/q2.jpg" />"/>  
                    </div>
                    <p align="center">
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-2').hide();
                                $('#quadro-1').show();">Anterior</button>
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-2').hide();
                                $('#quadro-3').show();">Próximo</button>
                    </p>
                </div>

                <div id="quadro-3" style="padding-top: 200px;">
                    <div class="quadros_atividades" >
                        <img width="350" height="350" src="<c:url value="/imagens/atividades/q3.jpg" />"/>  
                    </div>
                    <p align="center">
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-3').hide();
                                $('#quadro-2').show();">Anterior</button>
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-3').hide();
                                $('#quadro-4').show();">Próximo</button>
                    </p>
                </div>

                <div id="quadro-4" style="padding-top: 200px;">
                    <div class="quadros_atividades" >
                        <img width="350" height="350" src="<c:url value="/imagens/atividades/q4.jpg" />"/>  
                    </div>
                    <p align="center">
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-4').hide();
                                $('#quadro-3').show();">Anterior</button>
                        <button type="button" style="font-size: 12px;" class="botao_next atividade_button" onclick="$('#quadro-4').hide();
                                $('#quadro-final').show();">Próximo</button>
                    </p>
                </div>

                <div id="quadro-final" style="padding-top: 200px;">
                    <form  id="formValidarAtividade" onsubmit="return enviarResposta();">
                        <div class="quadros_respostas" style="float: left; padding-left: 50px;">
                            <img width="250" height="250" src="<c:url value="/imagens/atividades/q1.jpg" />"/>   
                            <select  class="input" style="width: 200px; height:35px " name="q1">
                                <option value="1">1º - Primeiro</option>
                                <option value="2">2º - Segundo</option>
                                <option value="3">3º - Terceiro</option>
                                <option value="4">4º - Quarto</option>
                            </select>          
                        </div>

                        <div class="quadros_respostas" style="float: left; padding-left: 50px;" >
                            <img width="250" height="250" src="<c:url value="/imagens/atividades/q4.jpg" />"/>
                            <select  class="input" style="width: 200px; height:35px " name="q2">
                                <option value="1">1º - Primeiro</option>
                                <option value="2">2º - Segundo</option>
                                <option value="3">3º - Terceiro</option>
                                <option value="4">4º - Quarto</option>
                            </select>
                        </div>

                        <div class="quadros_respostas" style="float: left; padding-left: 50px;">
                            <img width="250" height="250" src="<c:url value="/imagens/atividades/q2.jpg" />"/>
                            <select  class="input" style="width: 200px; height:35px " name="q3">
                                <option value="1">1º - Primeiro</option>
                                <option value="2">2º - Segundo</option>
                                <option value="3">3º - Terceiro</option>
                                <option value="4">4º - Quarto</option>
                            </select>
                        </div>

                        <div class="quadros_respostas" style="float: left; padding-left: 50px;" >
                            <img width="250" height="250" src="<c:url value="/imagens/atividades/q3.jpg" />"/> 
                            <select  class="input" style="width: 200px; height:35px " name="q4">
                                <option value="1">1º - Primeiro</option>
                                <option value="2">2º - Segundo</option>
                                <option value="3">3º - Terceiro</option>
                                <option value="4">4º - Quarto</option>
                            </select>
                        </div>

                        <p align="center">
                            <button type="button" style="font-size: 12px; margin-top: 100px;" class="botao_next atividade_button" onclick="$('#formValidarAtividade').submit();">Concluir</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Parabéns, Atividade Concluida!!</p>
        </div>
        <div id="dialog2" title="Tente Novamente!" style="display: none">
            <p>Resposta Incorreta, tente mais uma vez.</p>
        </div>
        <div id="rodape"><%@include file="../index/rodape.jsp" %></div>
    </body>
