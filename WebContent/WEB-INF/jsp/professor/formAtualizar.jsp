<%-- 
    Document   : formAtualizar
    Created on : 12/07/2014, 22:21:23
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script>
            function atualizarProfessor() {
                if ($("#formAtualizarProfessor").validate().form() === true) {
                    $.ajax({type: 'POST', url: '<c:url value="/professor/editar"/>', data: $('#formAtualizarProfessor').serialize(), success: function(response) {
                            $("#dialog").dialog({
                                buttons: {
                                    Ok: function() {
                                        $(this).dialog("close");
                                        $('#subConteudoLogado').load('<c:url value="/professor/formPerfil"/>');
                                    }
                                }

                            });

                        }});
                }
                return false;
            }
        </script>
    </head>
    <body>
        <div class="controladorAbas"">
            <div id="TituloAbas">
                <div class="abas" style="cursor: pointer;"> 
                    Perfil

                </div>
            </div>
            <div id="ConteudoAbas">
                <div style="width: 350px;">
                    <form  id="formAtualizarProfessor" onsubmit="return atualizarProfessor();">
                        <p style="width: 600px;" class="campo"> <label class="lab">Nome:</label><input class="input" value="${usuario.professor.nome}" name="professor.nome" style="padding-right: 40px; width: 400px;"></p>    
                        <p style="width: 400px;" class="campo"> <label class="lab">E-mail:</label><input value="${usuario.email}" name="professor.usuario.email" style="padding-right: 60px;" class="input"></p>
                        <input value="${usuarioSessao.usuario.login}" type="hidden" name="professor.usuario.login" style="padding-right: 60px;" class="input">
                        <input value="${usuarioSessao.usuario.login}" type="hidden" name="professor.loginProfessor" style="padding-right: 60px;" class="input">
                        <p style="width: 400px;" class="campo"><label class="lab" style="float: left;">Instituição de Ensino:</label><input name="professor.instituicao" style="padding-right: 60px;" value="${usuario.professor.instituicao}" class="input"></p>
                            <%--<p style="width: 400px;" class="campo"><label style="float: left;  padding-right: 110px;" class="lab"> Cargo:</label><input  style="float: left; width: 230px;" class="input"></p> --%>
                        <p></p>
                        <button type="button" class="botao" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000" onclick="$('#subConteudoLogado').load('<c:url value="/professor/formPerfil"/>');">Cancelar</button>
                        <button type="button" class="botao" onclick="$('#formAtualizarProfessor').submit();" style="float:right; width: 120px;">Atualizar</button>
                    </form>
                </div>
            </div>
            <div id="dialog" title="Sucesso!" style="display: none">
                <p>Atualização realizada com sucesso!</p>
            </div>
    </body>
    <script>
        $("#formAtualizarProfessor").validate({
            rules: {
                "professor.nome": {
                    required: true
                },
                "professor.usuario.email": {
                    required: true,
                    email: true
                },
                "professor.instituicao": {
                    required: true
                }
            }
        });
    </script>
</html>
