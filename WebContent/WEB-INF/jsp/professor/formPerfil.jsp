<%-- 
    Document   : formPerfil
    Created on : 05/06/2014, 17:00:14
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script>
            function removerProfessor() {
                $.ajax({type: 'POST', url: '<c:url value="/usuario/remover"/>', data: $('#formRemoverProfessor').serialize(), success: function(response) {
                        $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    javascript:location = '<c:url value="/"/>';
                                }
                            }

                        });

                    }});
                return false;
            }
        </script>
    </head>
    <body>
        <div class="controladorAbas"">
            <div id="TituloAbas">
                <div class="abas" style="cursor: pointer;"> 
                    Perfil

                </div>
            </div>
            <div id="ConteudoAbas">
                <label style="cursor: pointer; float: left; padding-top: 20px;">
                    <img width="180" height="150" src="<c:url value="/imagens/photo.jpg" />"/></label>
                <p class="campo"> 
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"> <b>Nome Completo:</b></label><br>
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"> ${usuario.professor.nome}</label>
                </p>    

                <p class="campo"> 
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"><b>E-mail para contato:</b></label><br>
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"> ${usuario.email}</label>
                </p>    

                <p class="campo"> 
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"><b>Instituição de Ensino:</b></label><br>
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"> ${usuario.professor.instituicao}</label>
                </p>    

                <p class="campo"> 
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"><b>Cargo:</b></label><br>
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"> Professor</label>
                </p>    

                <!--<p class="campo" style="padding-right: 300px;"> 
                    <label class="lab" style="color: #067AB7; padding-left: 10px;"><b>Total de turmas cadastradas:</b></label><br>
                    <label class="lab" style="color: #067AB7; padding-left: 10px;">0 turmas</label>
                </p>--> 
                <form  id="formRemoverProfessor" onsubmit="return removerProfessor();">
                    <input value="${usuarioSessao.usuario.login}" type="hidden" name="login" style="padding-right: 60px;" class="input">
                    <div style="padding-left: 180px; padding-top: 20px;">
                        <button  type="button" class="botao" style=" float: left; width: 120px;" onclick="$('#subConteudoLogado').load('<c:url value="/professor/formAtualizar"/>');">Editar</button>
                        <button type="button" class="botao" style=" float: left; width: 120px; background-color: #cc0000; border-color: #cc0000" onclick="$('#formRemoverProfessor').submit();">Excluir</button>

                    </div>
                </form>
            </div>
        </div>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Seus dados foram exluidos com sucesso!</p>
        </div>
    </body>
</html>
