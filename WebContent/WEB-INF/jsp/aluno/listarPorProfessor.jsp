<%-- 
    Document   : searchResult
    Created on : 19/08/2013, 15:41:40
    Author     : micael
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div>
            <div style="width: 510px;">
                <c:choose> 
                    <c:when test="${empty alunoList}">
                        Aluno não encontrado.
                    </c:when>
                    <c:otherwise>
                        <table id="userTable" style="width: 500px;">
                           <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Login</th>
                                    <th>Turma</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${alunoList}" var="aluno">
                                <tr>
                                    <td>${aluno.nome }</td>
                                    <td>${aluno.usuario.login}</td>
                                    <td>${aluno.idTurma.nome}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
