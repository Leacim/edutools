<%-- 
    Document   : formBuscar
    Created on : 03/06/2014, 03:48:34
    Author     : Micael
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/css/table.css" />" rel="stylesheet" type="text/css" />
        <script>
       function searchSubmit(){
        //document.surveyForm.submit();
         $.ajax({type:'POST', url: '<c:url value="/aluno/buscarPorNome"/>', data:$('#formBuscar').serialize(), success: function(response) {
                 $("#searchResult").html(response);
                }});
            return false;
       }
       
       function searchAllSubmit(){
         $.ajax({type:'POST', url: '<c:url value="/aluno/listarPorProfessor"/>', success: function(response) {
                 $("#searchResult").html(response);
                }});
    return false;
       }
       </script>
    </head>
    <body>
        <div style="width: 350px;">
            <form  id="formBuscar" onsubmit="return searchSubmit();" method="post">
            <p style="width: 600px;" class="campo"> <label class="lab">Login/Nome:</label><input name="nome" class="input" style="padding-right: 40px; width: 400px;"></p>
            <button type="button" class="botao" style="float:left; width: 160px; background-color: #00cccc; border-color: #00cccc" onclick="searchAllSubmit();">Listar Todos</button>
            <button type="button" class="botao" style="float:left; width: 120px;" onclick="$('#formBuscar').submit();">Buscar</button>
            </form>
        </div>
        <div id="searchResult" style="margin-top: 150px;">
            
        </div>
    </body>
</html>
