<%-- 
    Document   : formCriar
    Created on : 03/06/2014, 03:48:26
    Author     : Micael
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script>
            function cadastroAluno() {
                if($("#formCadastroAluno").validate().form() === true){
                $.ajax({type: 'POST', url: '<c:url value="/usuario/criar"/>', data: $('#formCadastroAluno').serialize(), success: function(response) {
                       $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    document.getElementById("formCadastroAluno").reset();
                                }
                            }

                        });
                        document.getElementById("formCadastroAluno").reset();
                    }});
            }
                return false;
            }
        </script>
    </head>
    <body>
                <div style="width: 350px;">
                    <form  id="formCadastroAluno" onsubmit="return cadastroAluno();">
                        <p style="width: 600px;" class="campo"> <label class="lab">Nome Completo:</label><input class="input" name="usuario.aluno.nome" style="padding-right: 40px; width: 380px;"></p>    
                        <p style="width: 400px;" class="campo"> <label class="lab">Login:</label><input name="usuario.login" style="padding-right: 60px;" class="input"></p>
                        <p style="width: 400px;" class="campo"><label class="lab" style="float: left;">Senha:</label><input name="usuario.senha" id="senha" type="password" style="padding-right: 60px;" class="input"></p>
                        <p style="width: 400px;" class="campo"><label style="float: left;  padding-right: 20px;" class="lab"> Confirmação de Senha:</label><input name="senhaConfirmacao" type="password"  style="padding-right: 60px;" class="input"></p>
                        <p style="width: 400px;" class="campo"><label class="lab" style="padding-right: 40px;">Turma:</label>
                            <select  class="input" style="width: 228px; height:35px " name="usuario.aluno.idTurma.idTurma">
                                <option value="">Nenhuma</option>
                                <c:forEach var="turma" items="${turmas}">
                                    <option value="${turma.idTurma}">${turma.nome}</option>                    
                                </c:forEach>
                            </select>
                        <p></p>

                        <input type="reset" value="Cancelar" class="botao" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000" />
                        <button type="button" class="botao" onclick="$('#formCadastroAluno').submit();" style="float:right; width: 120px;">Cadastrar</button>
                    </form>
                </div>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Cadastro realizado com sucesso!</p>
        </div>
    </body>
    <script>
        $("#formCadastroAluno").validate({
            rules: {
                "usuario.aluno.nome": {
                    required: true
                },
                "usuario.login": {
                    required: true
                },
                "usuario.senha": {
                    required: true
                },
                "senhaConfirmacao": {
                    equalTo: "#senha"
                },
                "usuario.aluno.idTurma.idTurma": {
                    required: true
                }
            }
        });
    </script>
</html>
