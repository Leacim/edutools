<%-- 
    Document   : formBuscar
    Created on : 14/06/2014, 14:50:21
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script>
            function cadastroTurma() {
                 if($("#formCadastroTurma").validate().form() === true){
                $.ajax({type: 'POST', url: '<c:url value="/turma/criar"/>', data: $('#formCadastroTurma').serialize(), success: function(response) {
                        $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    document.getElementById("formCadastroTurma").reset();
                                }
                            }

                        });

                    }
                });
            }
                return false;
            }
        </script>
    </head>
    <body>
                <div style="width: 350px;">
                    <form  id="formCadastroTurma" onsubmit="return cadastroTurma();">
                        <p style="width: 400px;" class="campo"> <label class="lab">Turma:</label><input name="turma.nome" style="padding-right: 60px;" class="input"></p>
                        <button type="button" class="botao" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000">Cancelar</button>
                        <button type="button" onclick="$('#formCadastroTurma').submit();" value="Cadastrar" class="botao" style="float:right; width: 120px;" >Cadastrar</button>
                    </form>
                </div>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Cadastro realizado com sucesso!</p>
        </div>
    </body>
    <script>
        $("#formCadastroTurma").validate({
                rules: {
                    "turma.nome": {
                        required: true
                    }
                }
            });
    </script>
</html>
