<%-- 
    Document   : loginAluno
    Created on : 27/06/2014, 16:13:06
    Author     : Samuel Romeiro
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edutools</title>
        <link href="<c:url value="/css/jquery-ui.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.theme.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
        <script src="<c:url value="/js/jquery.validate.js" />" type="text/javascript"></script>
        <script src="<c:url value="/js/jquery-ui.js" />" type="text/javascript"></script>

        <script>
            function loginSubmit() {
                if ($("#formLogin").validate().form() === true) {
                    $.ajax({type: 'POST', url: '<c:url value="/usuario/login"/>', data: $('#formLogin').serialize(),
                        success: function(response) {
                            if (response.usuario.tipo === 1) {
                                javascript:location = '<c:url value="/indexProfessorLogado" />';
                            } else {
                                javascript:location = '<c:url value="/atividades" />';
                            }
                        },
                        error: function() {
                            $("#dialog").dialog({
                                buttons: {
                                    Ok: function() {
                                        $(this).dialog("close");
                                    }
                                }

                            });
                        }
                    });
                }
                return false;
            }
        </script>
    </head>
    <body style="background-color: #0099ff; margin: 0px;">
        <div id="conteudo">
            <div id="sub-conteudo">
                <div style="float: left;">
                    <img style="cursor: pointer; float: left; padding-top: 70px; padding-left: 250px;" width="150" src="<c:url value="/imagens/Alunos.png" />"/>
                </div>
                <div style="float:left;" id="loginAluno">
                    <form id="formLogin" onsubmit="return loginSubmit();" method="post">
                        <p class="campo" style="padding-bottom: 20px">
                            <label class="lab" style="font:bold 25px Tahoma, Geneva, sans-serif; ">Login:</label>
                            <input name="usuario.login"  type="text" class="input" style="width: 250px;">
                        </p>
                        <p class="campo" style="padding-bottom: 20px">
                            <label class="lab" style="font:bold 25px Tahoma, Geneva, sans-serif;">Senha:</label>
                            <input name="usuario.senha" type="password" class="input" style="width: 250px;">
                        </p>
                        <button type="button" onclick="$('#formLogin').submit();" class="botao" style="margin-top: 20px; float: left;  width: 110px;"> <label style="cursor: pointer; font:bold 18px Tahoma, Geneva, sans-serif;">Entrar</label></button>
                        <button type="button" onclick="javascript:location = '<c:url value="/cadastroProfissional"/>'" class="botao" style="margin-top: 20px; float: left;  width: 120px;"> <label style="cursor: pointer; font:bold 18px Tahoma, Geneva, sans-serif;">Cadastrar</label></button>
                    </form>
                </div>
            </div>
        </div>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Login Inv�lido!</p>
        </div>
    </body>
    <script>
        $("#formLogin").validate({
            rules: {
                "usuario.login": {
                    required: true
                },
                "usuario.senha": {
                    required: true
                }
            }
        });
    </script>
</html>