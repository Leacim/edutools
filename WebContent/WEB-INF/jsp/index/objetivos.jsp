<%-- 
    Document   : conteudoCentro
     Created on : 16/05/2014, 12:39:23
    Author     : Micael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body style="background-color: #0099ff; margin: 0px;">
             <h1> EduTools </h1>

        <p>
            Analisando os caminhos que a educação inclusiva tem tomado no atual contexto, pudemos constatar que a procura por escolas especiais têm diminuindo na mesma proporção em que cresce o número de alunos com necessidades especiais em escolas regulares. A escola regular por tanto se abre a todos os alunos e assume o compromisso de apresentar recursos adequados e apoio a todos os que encontram barreiras no processo de ensino aprendizagem. A partir deste compromisso os profissionais da educação encontram novas barreiras, a da formação e dos recursos e materiais de apoio. Sabendo que a defasagem de materiais concretos é enorme, e da necessidade de inclusão digital, destacamos a grande deficiência de recursos digitais que atendam as diversas dificuldades de aprendizagem da atual escola regular.
        </p>
        <p> 
            Assim com “EduTools” pretendemos apresentar aos profissionais da educação um ambiente de auxílio na elaboração e realização de atividades digitais que beneficie a todos os públicos. Tal serviço tem como principal objetivo oferecer aos professores um ambiente simples para desenvolvimento de atividades que envolvam os aspectos de adaptação com o meio digital, o desenvolvimento de capacidades relacionadas ao uso da lógica e da percepção, e tratamento de informações. Vale salientar que as atividades propostas não são voltadas apenas para crianças com necessidades especiais, mas que são atividades que atendem a todos com suas especificidades, uma vez que qualquer adaptação não pode se tornar um plano paralelo do educador, mas deve ter por objetivo diferenciar os meios para igualar a participação.
        </p>
    </body>
</html>
