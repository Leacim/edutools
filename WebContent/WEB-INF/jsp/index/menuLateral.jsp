<%-- 
    Document   : menuLogado
    Created on : 29/05/2014, 16:17:16
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <ul class="menup">

        <li>
            <label style="cursor: pointer;" onclick="javascript:location = '<c:url value="/indexProfessorLogado" />';">
                <img width="30" src="<c:url value="/imagens/Home-icon.png" />"/> Inicio </label>
        </li>
        
        <li>
            <label style="cursor: pointer;" onclick="$('#subConteudoLogado').load('<c:url value="/professor/formPerfil"/>');">
                <img width="30" src="<c:url value="/imagens/7767_64x64.png" />"/> Perfil </label>
        </li>
        
        <li>
            <label style="cursor: pointer;" onclick="$('#subConteudoLogado').load('<c:url value="/aluno/formAluno"/>');">
                <img width="30" src="<c:url value="/imagens/7741_64x64.png" />"/> Alunos </label>
        </li>
        
        <li>
            <label style="cursor: pointer;" onclick="$('#subConteudoLogado').load('<c:url value="/turma/formTurma"/>');">
                <img width="30" src="<c:url value="/imagens/turma.jpg" />"/> Turmas </label>
        </li>
        
        <li>
            <label style="cursor: pointer;" onclick="$('#subConteudoLogado').load('<c:url value="/atividade/formAtividades"/>');">
                <img width="30" src="<c:url value="/imagens/Notas3.png" />"/> Atividades </label>
        </li>
        
        <li>
            <label style="cursor: pointer;" onclick="javascript:location = '<c:url value="/usuario/logout"/>';">
                <img width="30" src="<c:url value="/imagens/Sair.png" />"/> Sair </label>
        </li>
        
        </ul>
    </body>
</html>

