<%-- 
    Document   : conteudoCentro
     Created on : 16/05/2014, 12:39:23
    Author     : Micael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        
        <p id="formatacaoTexto" >
            Analisando os caminhos que a educação inclusiva tem tomado no atual 
            contexto pudemos constatar que a procura por escolas especiais
            têm diminuido na mesma proporção em que cresce o número de alunos 
            com necessidades especiais em escolas regulares. A escola regular 
            por tanto se abre a todos os alunos e assume o compromisso de 
            apresentar recursos adequados e apoio a todos os que encontram 
            barreiras no processo de ensino aprendizagem. A partir deste 
            compromisso os profissionais da educação encontram novas barreiras,
            a da formação e dos recursos e materiais de apoio. Sabendo que a 
            defasagem de materiais concretos é enorme, e da necessidade de 
            inclusão digital, destacamos a grande deficiência de recursos 
            digitais que atendam as diversas dificuldades de aprendizagem da
            atual escola regular. Assim com EduTools pretendemos apresentar
            aos profissionais da educação um auxílio na elaboração e realização
            de atividades digitais que beneficie a todos os públicos.
            </p>
       
    </body>
</html>
