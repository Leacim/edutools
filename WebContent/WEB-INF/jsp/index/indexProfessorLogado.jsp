<%-- 
    Document   : InicioLogado
    Created on : 29/05/2014, 14:42:57
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edutools</title>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.theme.css" />" rel="stylesheet" type="text/css"/>

        <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
        <script src="<c:url value="/js/jquery.validate.js" />" type="text/javascript"></script>
        <script src="<c:url value="/js/jquery-ui.js" />" type="text/javascript"></script>
    </head>
    <body style="background-color: #cccccc; margin: 0px;">
        <div id="conteudoLogado">
            <%@include file="../index/topoInf.jsp" %>
            <div id="menuLateral">
                <%@include file="../index/menuLateral.jsp" %></div>
            <div id="subConteudoLogado"><%--<%@include file="../index/abas.jsp" %>--%></div>
        </div>  
        <div id="rodape"><%@include file="../index/rodape.jsp" %></div>
    </body>
</html>