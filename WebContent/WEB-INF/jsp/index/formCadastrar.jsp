<%-- 
    Document   : formCadastrar
    Created on : 26/06/2014, 15:31:23
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="<c:url value="/js/jquery.validate.js" />" type="text/javascript"></script>
        <link href="<c:url value="/css/jquery-ui.css" />" rel="stylesheet" type="text/css"/>
        <link href="<c:url value="/css/jquery-ui.theme.css" />" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/js/jquery-ui.js" />" type="text/javascript"></script>
        <script>
            function cadastroSubmit() {
                 if($("#formCadastro").validate().form() === true){
                $.ajax({type: 'POST', url: '<c:url value="/usuario/criar"/>', data: $('#formCadastro').serialize(), success: function(response) {
                        $("#dialog").dialog({
                            buttons: {
                                Ok: function() {
                                    $(this).dialog("close");
                                    javascript:location = '<c:url value="/" />';
                                }
                            }

                        });
                        
                    }});
                    }
                return false;
            }
        </script>
    </head>
    <body>
        <form  id="formCadastro" onsubmit="return cadastroSubmit();"  style="height: 450px">
            <div id="formatacaoCadastro">
                <p style="width: 600px;" class="campo"> <label class="lab">Nome Completo:</label>
                    <input name="usuario.professor.nome" style="padding-right: 40px; width: 400px" class="input"></p>
                <p style="width: 400px;" class="campo"> <label class="lab">Login:</label>
                    <input name="usuario.login" style="padding-right: 40px; width: 200px" class="input"></p>
                <p style="width: 400px;" class="campo"><label class="lab" style="float: left;">Senha:</label>
                    <input name="usuario.senha" id="senha" type="password" style="padding-right: 40px; width: 200px" class="input"></p>
                <p style="width: 400px;" class="campo"><label class="lab" style="float: left;">Confirme sua Senha:</label>
                    <input  type="password" name="confirmaSenha"  style="padding-right: 40px; width: 200px" class="input"></p>
                <p style="width: 400px;" class="campo"> <label class="lab">E-mail:</label>
                    <input name="usuario.email" style="padding-right: 40px; width: 200px" class="input"></p>
                <p style="width: 400px; padding-bottom: 30px" class="campo"> <label class="lab">Instituição de Ensino:</label>
                    <input name="usuario.professor.instituicao" style="padding-right: 40px; width: 200px" class="input"></p>

                <button type="button" style="float:right; width: 120px; background-color: #cc0000; border-color: #cc0000"class="botao" onclick="javascript:location = '<c:url value="/"/>';">Cancelar</button>
                <button type="button" class="botao" onclick="$('#formCadastro').submit();" style="float:right; width: 120px;">Cadastrar</button>
            </div>
        </form>
        <div id="dialog" title="Sucesso!" style="display: none">
            <p>Cadastro realizado com sucesso!</p>
        </div>
    </body>
    <script>
        $("#formCadastro").validate({
            rules: {
                "usuario.professor.nome": {
                    required: true
                },
                "usuario.login": {
                    required: true
                },
                "usuario.senha": {
                    required: true
                },
                confirmaSenha: {
                    equalTo: "#senha"
                },
                "usuario.email": {
                    required: true,
                    email: true
                },
                "usuario.professor.instituicao": {
                    required: true
                }
            }
        });
    </script>

</html>
