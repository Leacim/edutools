<%-- 
    Document   : conteudoCentro
     Created on : 16/05/2014, 12:39:23
    Author     : Micael
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body style="background-color: #0099ff; margin: 0px;">
      <h1> Quem Somos </h1>
        <p>
            A equipe de desenvolvimento é composta por alunos do curso de mestrado em Ciência da Computação do Centro de Informática da Universidade Federal de Pernambuco (CIn-UFPE), com a parceria de uma profissional da área de educação, atuante nos ciclos de alfabetização do Ensino Fundamental. 
        </p>
        
        <h3> Dados da equipe de desenvolvimento do projeto: </h3>

        <p>Leonilson de Araújo Barboza (lab5@cin.ufpe.br)<br>
        Tester Development<br>
        <a href="http://lattes.cnpq.br/3608893947720844" target="_blank">Currículo Lattes</a>
        </p>
        
        <p>Dhielber Manzoli (dam5@cin.ufpe.br)<br>
        Tester Development <br>
        <a href="#" target="_blank">Currículo Lattes</a>
        </p>
        
        <p>Micael Soares de França (msf3@cin.ufpe.br)<br>
        Developer / Gerente<br>
        <a href="http://lattes.cnpq.br/0596100490066400" target="_blank"> Currículo Lattes</a>
        </p>
        
        <p>Samuel Carlos Romeiro Azevedo Souto (scras@cin.ufpe.br)<br>
        Developer / FrontEnd <br>
        <a href="http://lattes.cnpq.br/0431832283969677" target="_blank">Currículo Lattes</a>
        </p>

        <p>Ingridd Maurício Leite de Brito (imlb@cin.ufpe.br)<br>
        Tester Development<br>
        <a href="http://lattes.cnpq.br/9222876304599068" target="_blank">Currículo Lattes</a>
        </p>
        
        <p>Dayvison Santos de Almeida (designer@dayvisonsantos.com.br)<br>
        Designer / FrontEnd<br>
        <a href="http://lattes.cnpq.br/0683471011285026" target="_blank"> Currículo Lattes </a>
        </p>
        
        <p>
        Francielly Falcão (francielly.falcao@hotmail.com)<br>
        Consultora<br>
        Universidade Federal Rural de Pernambuco - UFRPE<br>
        Unidade Acadêmica de Garanhuns - UAG<br>
        <a href="http://lattes.cnpq.br/0363633459979946" target="_blank"> Currículo lattes </a>
        </p>
    </body>
</html>
