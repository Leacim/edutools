<%-- 
    Document   : topoLogado
    Created on : 29/05/2014, 15:42:52
    Author     : Samuel Romeiro
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div id="bannerLogado">
            <img style="cursor: pointer; padding-left: 20px;" width="350" height="110" src="<c:url value="/imagens/EduToolsAdmin.png" />" onclick="javascript:location = '<c:url value="/"/>';"/>
        
            <div id="avatar">
                <img width="60" height="55" style="float: left; padding-right: 5px;" src="<c:url value="/imagens/photo.jpg" />"/>
                <div style="float: left;">
                <label class="labeltopol" style="float: right">${usuarioSessao.usuario.professor.nome}</label><br>
                <label class="labeltopol" style="font-size: 12px;">${usuarioSessao.usuario.professor.instituicao}</label>
                </div>
        </div>
        </div>
     </body>
</html>
