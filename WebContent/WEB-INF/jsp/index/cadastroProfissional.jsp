<%-- 
    Document   : cadastroProfissional
    Created on : 26/06/2014, 12:03:46
    Author     : Samuel Romeiro
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edutools</title>
        <link href="<c:url value="/css/frame.css" />" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/js/jquery-1.10.2.min.js" />" type="text/javascript"></script>
    </head>
    <body  style="background-color: #0099ff; margin: 0px;">
        <div id="topo"><%@include file="../index/topoSemLogin.jsp" %></div>
        <div style="width: 100%; height: 10px; background-color: #0087ff"></div>  
        <div id="menuBotoes"> <%@include file="../index/menu.jsp" %> </div>
        <div id="conteudo">
            <div id="sub-conteudo" style="height: 580px;"><%@include file="../index/formCadastrar.jsp" %></div>
        </div>
        <div id="rodape"><%@include file="../index/rodape.jsp" %></div>
    </body>
</html>