/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.edutools.entities.Aluno;
import br.com.edutools.entities.Usuario;
import br.com.edutools.persist.RepositorioAluno;
import br.com.edutools.persist.RepositorioTurma;
import br.com.edutools.util.UsuarioSessao;
import java.util.List;

/**
 *
 * @author Micael
 */
@Resource
public class AlunoController {

    private RepositorioAluno alunos;
    private RepositorioTurma turmas;
    private UsuarioSessao usuarioSessao;
    private Result result;
    private Validator validator;
    
    public AlunoController(RepositorioAluno alunos, RepositorioTurma turmas,
            UsuarioSessao usuarioSessao, Result result, Validator validator) {
        this.alunos = alunos;
        this.turmas = turmas;
        this.usuarioSessao = usuarioSessao;
        this.result = result;
        this.validator = validator;
    }
    
    public void formAluno(){
        result.include("turmas", this.turmas.listar());
    }
    
    public void formCriar(){
    result.include("turmas", this.turmas.listar());
    }
    
    public void formBuscar(){}
    
   public void formAcompanhamento(){}
    
    
    
    public void criar(Aluno aluno) {
        this.alunos.criar(aluno);
    }

    public void editar(Aluno aluno) {
        this.alunos.editar(aluno);
    }

    public void remover(Integer id) {
        this.alunos.remover(id);
    }

    public List<Aluno> listar() {
        return this.alunos.listar();
    }

    public Aluno buscar(Integer id) {
        return this.alunos.buscar(id);
    }
    
    public List<Aluno> buscarPorNome(String nome){
        return this.alunos.buscarPorNome(nome, this.usuarioSessao.getUsuario().getLogin());
    }
    
    public List<Aluno> listarPorProfessor(){
        return this.alunos.listarPorProfessor(this.usuarioSessao.getUsuario().getLogin());
    }

}
