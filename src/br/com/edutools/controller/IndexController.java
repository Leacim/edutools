package br.com.edutools.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class IndexController {

	private final Result result;

	public IndexController(Result result) {
		this.result = result;
	}

	@Path("/")
	public void index() {
		
	}
        
        @Path("/indexProfessorLogado")
        public void indexProfessorLogado(){
            
        }
        
        @Path("/eduInclusiva")
        public void eduInclusiva(){
            
        }

        @Path("/cadastroProfissional")
        public void cadastroProfissional(){
            
        }
        @Path("/quemSomos")
        public void quemSomos(){
            
        }
        
        @Path("/loginAluno")
        public void loginAluno(){
        
        }
        
          @Path("/objetivos")
        public void objetivos(){
            
        }
}
