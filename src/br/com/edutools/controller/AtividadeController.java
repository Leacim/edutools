/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.edutools.entities.Atividade;
import br.com.edutools.entities.Turma;
import br.com.edutools.persist.RepositorioAtividade;
import br.com.edutools.persist.RepositorioTurma;

/**
 *
 * @author Micael
 */
@Resource
public class AtividadeController {
    
    private RepositorioAtividade atividades;
    private RepositorioTurma turmas;
    private Result result;
    
    public AtividadeController(RepositorioAtividade atividades,
            RepositorioTurma turmas, Result result){
        this.atividades = atividades;
        this.turmas = turmas;
        this.result = result;
    }
    
    public void formAtividades(){
         result.include("turmas", this.turmas.listar());
          result.include("atividades", this.atividades.listar());
        
    }
    
    public void formExcluir(){
     result.include("atividades", this.atividades.listar());
    }
    
     public void formDisponibilizar(){
            result.include("turmas", this.turmas.listar());
          result.include("atividades", this.atividades.listar());
    
    }
    
     @Path("/atividades")
        public void atividades(){
            
        }
        public void historia(){
            
        }
        
        public void disponibilizar(Atividade atividade, Turma turma){
            atividade = this.atividades.buscar(atividade.getIdAtividade());
            atividade.setIdTurma(turma);
            this.atividades.editar(atividade);
            result.nothing();
        }
        
        public void remover(int id){
            this.atividades.remover(id);
        }
        
        public void validarHistoria(int q1, int q2, int q3, int q4){
            if(q1 == 1 && q2 ==4 && q3 == 2 && q4==3){
                 result.nothing();
            }else{
                result.use(Results.http()).sendError(0);
            }
        }
}
