/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.edutools.controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import static br.com.caelum.vraptor.view.Results.json;
import br.com.edutools.controller.IndexController;
import br.com.edutools.entities.Aluno;
import br.com.edutools.entities.Professor;
import br.com.edutools.entities.Usuario;
import br.com.edutools.persist.RepositorioUsuario;
import br.com.edutools.util.Upload;
import br.com.edutools.util.UsuarioSessao;
import java.util.List;

/**
 *
 * @author Micael
 */
@Resource
public class UsuarioController {

    private RepositorioUsuario usuarios;
    private UsuarioSessao usuarioSessao;
    private Result result;
    private Validator validator;
    private Upload upload;

    public UsuarioController(RepositorioUsuario usuarios, UsuarioSessao usuarioSessao,
            Result result, Validator validator, Upload upload) {
        this.usuarios = usuarios;
        this.usuarioSessao = usuarioSessao;
        this.result = result;
        this.validator = validator;
        this.upload = upload;
    }

    public void criar(Usuario usuario) {
        Professor professor = usuario.getProfessor();
       if(professor !=null){
           usuario.setTipo(1);
           professor.setLoginProfessor(usuario.getLogin());
           usuario.setProfessor(professor);
       }else{
           Aluno aluno = usuario.getAluno();
           if(aluno !=null){
               usuario.setTipo(2);
               aluno.setLoginAluno(usuario.getLogin());
               usuario.setAluno(aluno);
           }
       } 
        this.usuarios.criar(usuario);
        result.nothing();
    }

    public void editar(Usuario usuario) {
        this.usuarios.editar(usuario);
    }

    public void remover(String login) {
        this.usuarios.remover(this.usuarioSessao.getUsuario());
        result.nothing();
    }

    public List<Usuario> listar() {
        return this.usuarios.listar();
    }

    public Usuario buscar(String login) {
        return this.usuarios.buscar(login);
    }

    public void login(Usuario usuario) {
        usuario = this.usuarios.login(usuario);
        if (usuario != null) {
            this.usuarioSessao.setUsuario(usuario);
            //result.redirectTo(IndexController.class).indexProfessorLogado();
            result.use(json()).from(usuario).serialize();
            
        } else {
         //   validator.add(new ValidationMessage("usuario não encontrado", "usuario não encontrado"));
          //  validator.onErrorUsePageOf(IndexController.class).index();
       result.use(Results.http()).sendError(0);
        }
    }

    public void logout() {
        this.usuarioSessao.setUsuario(null);
        result.redirectTo(IndexController.class).index();
    }
    
     public void enviarDocumentacao(UploadedFile doc, String userId) {
   
        if (doc != null) {
            this.upload.salvarDocumento(doc, userId);
        }
        result.use(Results.json()).from(doc).serialize();
    }

}
