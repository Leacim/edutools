/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.edutools.controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.edutools.entities.Professor;
import br.com.edutools.entities.Usuario;
import br.com.edutools.persist.RepositorioProfessor;
import br.com.edutools.persist.RepositorioUsuario;
import br.com.edutools.util.UsuarioSessao;
import java.util.List;

/**
 *
 * @author Samuel Romeiro
 */
@Resource
public class ProfessorController {

    private RepositorioProfessor professores;
    private RepositorioUsuario usuarios;
    private UsuarioSessao usuarioSessao;
    private Result result;

    public ProfessorController(RepositorioProfessor professores,
            RepositorioUsuario usuarios,
            UsuarioSessao usuarioSessao, Result result) {
        this.professores = professores;
        this.usuarios = usuarios;
        this.usuarioSessao = usuarioSessao;
        this.result = result;
    }

    public void formPerfil() {
        Usuario usuario = this.usuarios.buscar(this.usuarioSessao.getUsuario().getLogin());
        result.include("usuario", usuario);
    }

    public void formAtualizar() {
        Usuario usuario = this.usuarios.buscar(this.usuarioSessao.getUsuario().getLogin());
        result.include("usuario", usuario);
    }

    public void criar(Professor professor) {
        this.professores.criar(professor);
    }

    public void editar(Professor professor) {
        Usuario usuario = professor.getUsuario();
        usuario.setSenha(this.usuarioSessao.getUsuario().getSenha());
        usuario.setTipo(1);
        usuario.setProfessor(professor);
        professor.setUsuario(usuario);
        
        this.professores.editar(professor);
        result.nothing();

    }

    public void remover(Integer id) {
        this.professores.remover(id);
    }

    public List<Professor> listar() {
        return this.professores.listar();
    }

    public Professor buscar(String id) {
        return this.professores.buscar(id);
    }

}
