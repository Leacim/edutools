/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.controller;

import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.edutools.entities.Turma;
import br.com.edutools.persist.RepositorioTurma;
import br.com.edutools.util.UsuarioSessao;
import java.util.List;

/**
 *
 * @author Samuel Romeiro
 */
@Resource
public class TurmaController {

    private RepositorioTurma turmas;
     private UsuarioSessao usuarioSessao;
    private Result result;
    private Validator validator;
    
    public TurmaController(RepositorioTurma turmas, UsuarioSessao usuarioSessao,
            Result result, Validator validator) {
        this.turmas = turmas;
        this.usuarioSessao = usuarioSessao;
        this.result = result;
        this.validator = validator;
    }
    
    public void formTurma(){
        
    }
    
    public void formCriar(){}
    
    public void formBuscar(){}
    
    public void criar(Turma turma){
        turma.setLoginProfessor( this.usuarioSessao.getUsuario().getProfessor());
        this.turmas.criar(turma);
        result.nothing();
    }
    
    public void editar(Turma turma){
        this.turmas.editar(turma);
    }
    
    public void remover(int id){
        this.turmas.remover(id);
    }
    
    public List<Turma> listar(){
        return this.turmas.listar();
    }
    
    public Turma buscar(int id){
        return this.turmas.buscar(id);
    }
    
    public List<Turma> buscarPorNome(Turma turma){
        turma.setLoginProfessor(this.usuarioSessao.getUsuario().getProfessor());
        return this.turmas.buscarPorNome(turma);
    }
    
     public List<Turma> buscarPorProfessor(String login){
         login = this.usuarioSessao.getUsuario().getLogin();
        return this.turmas.buscarPorProfessor(login);
    }
}
