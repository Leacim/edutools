/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.util;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;
import br.com.edutools.entities.Usuario;

/**
 *
 * @author Micael
 */

@Component
@SessionScoped
public class UsuarioSessao {
      private Usuario user;
     
    public Usuario getUsuario() {
        return user;
    }
 
    public void setUsuario(Usuario usuario) {
        this.user = usuario;
    }
    
}
