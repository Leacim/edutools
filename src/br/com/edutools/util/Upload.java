/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.edutools.util;

import br.com.caelum.vraptor.interceptor.multipart.UploadedFile;
import br.com.caelum.vraptor.ioc.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.apache.tomcat.util.http.fileupload.IOUtils;



/**
 * Classe responsável pelo envio de arquivos ao servidor
 * @author micael
 */
@Component
public class Upload {
    
    private File pastaImagens;
    private ServletContext contextPath;
    
    /**
     * Contrutor da classe Upload
     * @param context Contexto do caminho da aplicação
     */
    public Upload(ServletContext context){
        this.contextPath = context;
    }
    
    /**
     * Método que cria uma pasta no caminho especificado
     * @param caminho String que representa o path
     */
    private void criarPasta(String caminho){
        this.pastaImagens = new File(caminho);
        this.pastaImagens.mkdir();
    }
 
    /**
     * Método para deletar um pasta e seu conteúdo a partir de um caminho especificado
     * @param caminho String que representa o path
     */
    public void deletarPasta(String caminho){
        File pasta = new File(this.contextPath.getRealPath(caminho));
        pasta.delete();
    }           
    
    /**
     * Método que cópia o arquivo envio para o caminho definido
     * @param doc arquivo enviado
     * @param idUser id do usuario que envio o arquivo
     * @param idSolicitacao id da solicitação referente ao arquivo enviado
     */
    public void salvarDocumento(UploadedFile doc, String idUser){
        System.out.println(this.contextPath.getRealPath("/documentos/user/" + idUser));
     this.criarPasta(this.contextPath.getRealPath("/documentos/user/" + idUser)); 
    // this.criarPasta(this.contextPath.getRealPath("/documentos/user/" + idUser +"/"+idSolicitacao)); 
     File destino = new File(this.pastaImagens, doc.getFileName());
        try {
                IOUtils.copyLarge(doc.getFile(), new FileOutputStream(destino));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Upload.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex){
                Logger.getLogger(Upload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Método que verifica se um determinado arquivo existe, a partir de seu nome
     * e localização(Path)
     * 
     * @param nome String do nome do arquivo
     * @param path String do caminho do arquivo
     * @return retorna true caso existe e false caso contrário
     */
    public boolean existe(String nome, String path){
        boolean resp = true;
        String caminho = this.contextPath.getRealPath(path);
         File imagem = new File(caminho, nome);
         FileInputStream arquivo;
        try {
            arquivo = new FileInputStream(imagem);
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(Imagens.class.getName()).log(Level.SEVERE, null, ex);
            resp = false;
        }
        return resp;
    }
    
    /**
     * Método que lista o nome de um arquivo de uma pasta
     * @param path String do caminho da pasta
     * @return retorna nome do arquivo
     */
     public String listarArquivos(String path){
        String resp = null;
        String caminho = this.contextPath.getRealPath(path);
         File arq = new File(caminho);
         File[] files = arq.listFiles();
         if(files !=null){
             resp = files[0].getName();
         }
         return resp;
    }
}
