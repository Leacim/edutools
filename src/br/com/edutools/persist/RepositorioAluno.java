/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.edutools.persist;

import br.com.caelum.vraptor.ioc.Component;
import br.com.edutools.entities.Aluno;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Micael
 */
@Component
public class RepositorioAluno {

    public RepositorioAluno() {
        this.emf = Persistence.createEntityManagerFactory("EduToolsPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void criar(Aluno aluno) {
        EntityManager em = null;
        em = this.getEntityManager();
        em.getTransaction().begin();
        em.persist(aluno);
        em.getTransaction().commit();
    }

    public void editar(Aluno aluno) {
        EntityManager em = null;
        em = this.getEntityManager();
        em.getTransaction().begin();
        em.merge(aluno);
        em.getTransaction().commit();
    }

    public void remover(Integer id) {
        EntityManager em = null;
        em = this.getEntityManager();
        em.getTransaction().begin();
        em.remove(id);
        em.getTransaction().commit();
    }

    public List<Aluno> listar() {
        EntityManager em = getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Aluno.class));
        Query q = em.createQuery(cq);
        return q.getResultList();
    }

    public Aluno buscar(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Aluno.class, id);
        } finally {
            em.close();
        }
    }

    public List<Aluno> buscarPorNome(String nome, String login) {
        EntityManager em = getEntityManager();
        String query = " ";
        query = "select c from Aluno c where c.idTurma.loginProfessor.loginProfessor = :login and c.nome like :nome";
        nome = "%" + nome + "%";

        Query q = em.createQuery(query);
        q.setParameter("login", login);
        q.setParameter("nome", nome);
        return q.getResultList();
    }
    
    public List<Aluno> listarPorProfessor(String login) {
        EntityManager em = getEntityManager();
        String query = " ";
        query = "select c from Aluno c where c.idTurma.loginProfessor.loginProfessor = :login";

        Query q = em.createQuery(query);
        q.setParameter("login", login);
        return q.getResultList();
    }
}
