/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.persist;

import br.com.caelum.vraptor.ioc.Component;
import br.com.edutools.entities.Professor;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Micael
 */
@Component
public class RepositorioProfessor {
    
    public RepositorioProfessor() {
        this.emf = Persistence.createEntityManagerFactory("EduToolsPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void criar(Professor professor){
         EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.persist(professor);
        em.getTransaction().commit();
    }
    
    public void editar(Professor professor){
            EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.merge(professor);
        em.getTransaction().commit();
    }
    
    public void remover(Integer id){
          EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.remove(id);
        em.getTransaction().commit();
    }
    
    public List<Professor> listar(){
        EntityManager em = getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Professor.class));
        Query q = em.createQuery(cq);
        return q.getResultList();
    }
    
    public Professor buscar(String id){
          EntityManager em = getEntityManager();
        try {
            return em.find(Professor.class, id);
        } finally {
            em.close();
        }
    }
}
