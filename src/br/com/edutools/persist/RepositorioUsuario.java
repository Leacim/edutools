/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.edutools.persist;

import br.com.caelum.vraptor.ioc.Component;
import br.com.edutools.entities.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Micael
 */
@Component
public class RepositorioUsuario implements Serializable {

    public RepositorioUsuario() {
        this.emf = Persistence.createEntityManagerFactory("EduToolsPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void criar(Usuario usuario) {
        EntityManager em = null;
        em = this.getEntityManager();
        em.getTransaction().begin();
        em.persist(usuario);
        em.getTransaction().commit();
    }

    public void editar(Usuario usuario) {
        EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.merge(usuario);
        em.getTransaction().commit();
    }

    public void remover(Usuario login) {
        EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.remove(em.merge(login));
        em.getTransaction().commit();
    }

    public List<Usuario> listar() {
        EntityManager em = getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Usuario.class));
        Query q = em.createQuery(cq);
        return q.getResultList();
    }
    
    public Usuario buscar(String login){
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, login);
        } finally {
            em.close();
        }
    }
    
    public Usuario login(Usuario usuario){
          EntityManager em = getEntityManager();
        Usuario userTemp = null;
        Query q = em.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha");
        q.setParameter("login", usuario.getLogin());
        q.setParameter("senha", usuario.getSenha());
         
        List listaUsuarios = q.getResultList();
        if(!listaUsuarios.isEmpty()){
            userTemp =  (Usuario) listaUsuarios.get(0);
        }     
        return userTemp;
    }
}
