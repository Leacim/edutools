/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.persist;

import br.com.caelum.vraptor.ioc.Component;
import br.com.edutools.entities.Turma;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Micael
 */
@Component
public class RepositorioTurma implements Serializable{
    
    
    public RepositorioTurma() {
        this.emf = Persistence.createEntityManagerFactory("EduToolsPU");
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void criar(Turma turma){
         EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.persist(turma);
        em.getTransaction().commit();
    }
    
    public void editar(Turma turma){
            EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.merge(turma);
        em.getTransaction().commit();
    }
    
    public void remover(Integer id){
          EntityManager em = null;
        em = this.getEntityManager();
         em.getTransaction().begin();
        em.remove(id);
        em.getTransaction().commit();
    }
    
    public List<Turma> listar(){
        EntityManager em = getEntityManager();
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Turma.class));
        Query q = em.createQuery(cq);
        return q.getResultList();
    }
    
    public Turma buscar(Integer id){
          EntityManager em = getEntityManager();
        try {
            return em.find(Turma.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<Turma> buscarPorNome(Turma turma) {
        EntityManager em = getEntityManager();
        String query = " ";
        query = "select c from Turma c where c.loginProfessor.loginProfessor = :login and c.nome like :nome";
        turma.setNome("%" + turma.getNome() + "%");

        Query q = em.createQuery(query);
        q.setParameter("login", turma.getLoginProfessor().getLoginProfessor());
        q.setParameter("nome", turma.getNome());
        return q.getResultList();
    }
    
    public List<Turma> buscarPorProfessor(String login) {
        EntityManager em = getEntityManager();
        String query = " ";
        query = "select c from Turma c where c.loginProfessor.loginProfessor = :login";
       
        Query q = em.createQuery(query);
        q.setParameter("login", login);
  
        return q.getResultList();
    }
}
