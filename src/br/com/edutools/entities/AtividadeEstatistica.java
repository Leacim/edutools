/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Micael
 */
@Entity
@Table(name = "atividade_estatistica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtividadeEstatistica.findAll", query = "SELECT a FROM AtividadeEstatistica a"),
    @NamedQuery(name = "AtividadeEstatistica.findByLoginAluno", query = "SELECT a FROM AtividadeEstatistica a WHERE a.atividadeEstatisticaPK.loginAluno = :loginAluno"),
    @NamedQuery(name = "AtividadeEstatistica.findByIdAtividade", query = "SELECT a FROM AtividadeEstatistica a WHERE a.atividadeEstatisticaPK.idAtividade = :idAtividade"),
    @NamedQuery(name = "AtividadeEstatistica.findByIdAtividadeEstatistica", query = "SELECT a FROM AtividadeEstatistica a WHERE a.atividadeEstatisticaPK.idAtividadeEstatistica = :idAtividadeEstatistica"),
    @NamedQuery(name = "AtividadeEstatistica.findByTempo", query = "SELECT a FROM AtividadeEstatistica a WHERE a.tempo = :tempo"),
    @NamedQuery(name = "AtividadeEstatistica.findByAcertos", query = "SELECT a FROM AtividadeEstatistica a WHERE a.acertos = :acertos"),
    @NamedQuery(name = "AtividadeEstatistica.findByData", query = "SELECT a FROM AtividadeEstatistica a WHERE a.data = :data")})
public class AtividadeEstatistica implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AtividadeEstatisticaPK atividadeEstatisticaPK;
    @Column(name = "tempo")
    @Temporal(TemporalType.TIME)
    private Date tempo;
    @Column(name = "acertos")
    private Integer acertos;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @JoinColumn(name = "login_aluno", referencedColumnName = "login_aluno", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Aluno aluno;
    @JoinColumn(name = "id_atividade", referencedColumnName = "id_atividade", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Atividade atividade;

    public AtividadeEstatistica() {
    }

    public AtividadeEstatistica(AtividadeEstatisticaPK atividadeEstatisticaPK) {
        this.atividadeEstatisticaPK = atividadeEstatisticaPK;
    }

    public AtividadeEstatistica(AtividadeEstatisticaPK atividadeEstatisticaPK, Date data) {
        this.atividadeEstatisticaPK = atividadeEstatisticaPK;
        this.data = data;
    }

    public AtividadeEstatistica(String loginAluno, int idAtividade, int idAtividadeEstatistica) {
        this.atividadeEstatisticaPK = new AtividadeEstatisticaPK(loginAluno, idAtividade, idAtividadeEstatistica);
    }

    public AtividadeEstatisticaPK getAtividadeEstatisticaPK() {
        return atividadeEstatisticaPK;
    }

    public void setAtividadeEstatisticaPK(AtividadeEstatisticaPK atividadeEstatisticaPK) {
        this.atividadeEstatisticaPK = atividadeEstatisticaPK;
    }

    public Date getTempo() {
        return tempo;
    }

    public void setTempo(Date tempo) {
        this.tempo = tempo;
    }

    public Integer getAcertos() {
        return acertos;
    }

    public void setAcertos(Integer acertos) {
        this.acertos = acertos;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Atividade getAtividade() {
        return atividade;
    }

    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (atividadeEstatisticaPK != null ? atividadeEstatisticaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtividadeEstatistica)) {
            return false;
        }
        AtividadeEstatistica other = (AtividadeEstatistica) object;
        if ((this.atividadeEstatisticaPK == null && other.atividadeEstatisticaPK != null) || (this.atividadeEstatisticaPK != null && !this.atividadeEstatisticaPK.equals(other.atividadeEstatisticaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.edutools.entities.AtividadeEstatistica[ atividadeEstatisticaPK=" + atividadeEstatisticaPK + " ]";
    }
    
}
