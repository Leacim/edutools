/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Micael
 */
@Entity
@Table(name = "aluno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aluno.findAll", query = "SELECT a FROM Aluno a"),
    @NamedQuery(name = "Aluno.findByLoginAluno", query = "SELECT a FROM Aluno a WHERE a.loginAluno = :loginAluno"),
    @NamedQuery(name = "Aluno.findByNome", query = "SELECT a FROM Aluno a WHERE a.nome = :nome"),
    @NamedQuery(name = "Aluno.findBySexo", query = "SELECT a FROM Aluno a WHERE a.sexo = :sexo"),
    @NamedQuery(name = "Aluno.findByDataNascimento", query = "SELECT a FROM Aluno a WHERE a.dataNascimento = :dataNascimento"),
    @NamedQuery(name = "Aluno.findByPortadorNessEsp", query = "SELECT a FROM Aluno a WHERE a.portadorNessEsp = :portadorNessEsp")})
public class Aluno implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "login_aluno")
    private String loginAluno;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Column(name = "sexo")
    private Integer sexo;
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    @Basic(optional = false)
    @Column(name = "portador_ness_esp")
    private boolean portadorNessEsp;
    @JoinColumn(name = "login_aluno", referencedColumnName = "login", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Usuario usuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aluno")
    private List<AtividadeEstatistica> atividadeEstatisticaList;
     @JoinColumn(name = "id_turma", referencedColumnName = "id_turma")
    @ManyToOne(optional = false)
    private Turma idTurma;

    public Aluno() {
    }

    public Aluno(String loginAluno) {
        this.loginAluno = loginAluno;
    }

    public Aluno(String loginAluno, String nome, boolean portadorNessEsp) {
        this.loginAluno = loginAluno;
        this.nome = nome;
        this.portadorNessEsp = portadorNessEsp;
    }

    public String getLoginAluno() {
        return loginAluno;
    }

    public void setLoginAluno(String loginAluno) {
        this.loginAluno = loginAluno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getSexo() {
        return sexo;
    }

    public void setSexo(Integer sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public boolean getPortadorNessEsp() {
        return portadorNessEsp;
    }

    public void setPortadorNessEsp(boolean portadorNessEsp) {
        this.portadorNessEsp = portadorNessEsp;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @XmlTransient
    public List<AtividadeEstatistica> getAtividadeEstatisticaList() {
        return atividadeEstatisticaList;
    }

    public void setAtividadeEstatisticaList(List<AtividadeEstatistica> atividadeEstatisticaList) {
        this.atividadeEstatisticaList = atividadeEstatisticaList;
    }

     public Turma getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(Turma idTurma) {
        this.idTurma = idTurma;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginAluno != null ? loginAluno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aluno)) {
            return false;
        }
        Aluno other = (Aluno) object;
        if ((this.loginAluno == null && other.loginAluno != null) || (this.loginAluno != null && !this.loginAluno.equals(other.loginAluno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.edutools.entities.Aluno[ loginAluno=" + loginAluno + " ]";
    }
    
}
