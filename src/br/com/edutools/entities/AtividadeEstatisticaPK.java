/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.edutools.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Micael
 */
@Embeddable
public class AtividadeEstatisticaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "login_aluno")
    private String loginAluno;
    @Basic(optional = false)
    @Column(name = "id_atividade")
    private int idAtividade;
    @Basic(optional = false)
    @Column(name = "id_atividade_estatistica")
    private int idAtividadeEstatistica;

    public AtividadeEstatisticaPK() {
    }

    public AtividadeEstatisticaPK(String loginAluno, int idAtividade, int idAtividadeEstatistica) {
        this.loginAluno = loginAluno;
        this.idAtividade = idAtividade;
        this.idAtividadeEstatistica = idAtividadeEstatistica;
    }

    public String getLoginAluno() {
        return loginAluno;
    }

    public void setLoginAluno(String loginAluno) {
        this.loginAluno = loginAluno;
    }

    public int getIdAtividade() {
        return idAtividade;
    }

    public void setIdAtividade(int idAtividade) {
        this.idAtividade = idAtividade;
    }

    public int getIdAtividadeEstatistica() {
        return idAtividadeEstatistica;
    }

    public void setIdAtividadeEstatistica(int idAtividadeEstatistica) {
        this.idAtividadeEstatistica = idAtividadeEstatistica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginAluno != null ? loginAluno.hashCode() : 0);
        hash += (int) idAtividade;
        hash += (int) idAtividadeEstatistica;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtividadeEstatisticaPK)) {
            return false;
        }
        AtividadeEstatisticaPK other = (AtividadeEstatisticaPK) object;
        if ((this.loginAluno == null && other.loginAluno != null) || (this.loginAluno != null && !this.loginAluno.equals(other.loginAluno))) {
            return false;
        }
        if (this.idAtividade != other.idAtividade) {
            return false;
        }
        if (this.idAtividadeEstatistica != other.idAtividadeEstatistica) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.edutools.entities.AtividadeEstatisticaPK[ loginAluno=" + loginAluno + ", idAtividade=" + idAtividade + ", idAtividadeEstatistica=" + idAtividadeEstatistica + " ]";
    }
    
}
